import * as React from "react";

import { createSlice, configureStore } from "@reduxjs/toolkit";
import { Provider, useDispatch, useSelector } from "react-redux";

// bikin reducer
const ticTacToe = createSlice({
  name: "ticTacToe",
  initialState: {
    squares: Array(9).fill(null),
    winner: null,
    nextValue: "X",
    status: "Next player: X",
  },
  reducers: {
    selectSquare(state, action) {
      if (!state.winner && !state.squares[action.payload]) {
        const newSquares = [...state.squares];
        newSquares[action.payload] = calculateNextValue(state.squares);
        const winner = calculateWinner(newSquares);
        const nextValue = calculateNextValue(newSquares);
        const status = calculateStatus(winner, newSquares, nextValue);
        return {
          squares: newSquares,
          winner,
          status,
        };
      }
    },
    restartGame(state, action) {
      return {
        squares: Array(9).fill(null),
        winner: null,
        nextValue: "X",
        status: "Next player: X",
      };
    },
  },
});

export const { selectSquare } = ticTacToe.actions;
export const { restartGame } = ticTacToe.actions;

// store
const store = configureStore({
  reducer: ticTacToe.reducer,
});

function Board() {
  const { status, squares } = useSelector((state) => state);
  const dispatch = useDispatch();
  function selectSquareHandler(squareIndex) {
    dispatch(selectSquare(squareIndex));
  }

  function handleRestart() {
    dispatch(restartGame());
  }

  function renderSquare(i) {
    return (
      <button
        className='align-top m-2 h-20 w-20 shadow-lg rounded-lg bg-slate-300 hover:bg-slate-400'
        onClick={() => selectSquareHandler(i)}
      >
        {squares[i]}
      </button>
    );
  }

  return (
    <div className='flex flex-col'>
      <h1 className='font-bold text-2xl mx-auto mt-5'>Tic Tac Toe Game</h1>
      <p className='font-semibold text-lg mx-auto my-2'>{status}</p>
      <div className='shadow-2xl rounded-xl mx-auto p-2'>
        <div className='mx-auto'>
          {renderSquare(0)}
          {renderSquare(1)}
          {renderSquare(2)}
        </div>
        <div className='mx-auto'>
          {renderSquare(3)}
          {renderSquare(4)}
          {renderSquare(5)}
        </div>
        <div className='mx-auto'>
          {renderSquare(6)}
          {renderSquare(7)}
          {renderSquare(8)}
        </div>
      </div>
      <button
        className='my-4 mx-auto px-3 py-1 bg-slate-700 rounded-2xl text-white text-sm font-semibold hover:bg-slate-500'
        onClick={handleRestart}
      >
        Restart
      </button>
    </div>
  );
}

function Game() {
  return (
    <div className='mt-8 w-4/12 mx-auto bg-slate-200 rounded-lg'>
      <Board />
    </div>
  );
}

// eslint-disable-next-line no-unused-vars
function calculateStatus(winner, squares, nextValue) {
  return winner
    ? `Winner: ${winner}`
    : squares.every(Boolean)
    ? `Scratch: Cat's game`
    : `Next player: ${nextValue}`;
}

// eslint-disable-next-line no-unused-vars
function calculateNextValue(squares) {
  return squares.filter(Boolean).length % 2 === 0 ? "X" : "O";
}

// eslint-disable-next-line no-unused-vars
function calculateWinner(squares) {
  const lines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ];
  for (let i = 0; i < lines.length; i++) {
    const [a, b, c] = lines[i];
    if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
      return squares[a];
    }
  }
  return null;
}

function App() {
  return (
    <Provider store={store}>
      <Game />
    </Provider>
  );
}

export default App;
